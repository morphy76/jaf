export default class AbstractValidator {
	constructor(cfg = {}) {
		this.cfg = cfg;
	}
	check(value) {
		throw new Error('Cannot invoke an abstract method');
	}
}
