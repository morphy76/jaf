import AbstractValidator from './abstract_validator';

export default class RegExValidator extends AbstractValidator {
	check(value) {
		return new RegExp(this.cfg.expression).test(value) ? null : this.cfg.err;
	}
}
