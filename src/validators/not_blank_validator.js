import RequiredValidator from './required_validator';

export default class NotBlankValidator extends RequiredValidator {
	check(value) {
		let superCheck = super.check(value);
		if(superCheck === null) {
			superCheck = value.trim().length > 0 ? null : this.cfg;
		}
		return superCheck;
	}
}
