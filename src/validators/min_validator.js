import IsDigitValidator from './is_digit_validator';

export default class MinValidator extends IsDigitValidator {
	check(value) {
		let superCheck = super.check(value);
		if(superCheck === null) {
			superCheck = value > this.cfg.min ? null : this.cfg.err;
		}
		return superCheck;
	}
}
