import NotBlankValidator from './not_blank_validator';

export default class IsDigitValidator extends NotBlankValidator {
	check(value) {
		let superCheck = super.check(value);
		if(superCheck === null) {
			superCheck = /^\d+$/.test(value) ? null : this.cfg;
		}
		return superCheck;
	}
}
