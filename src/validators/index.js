import RequiredValidator from './required_validator';
import NotBlankValidator from './not_blank_validator';
import IsDigitValidator from './is_digit_validator';
import MinValidator from './min_validator';
import MaxValidator from './max_validator';
import IsDateValidator from './is_date_validator';
import RegExValidator from './regex_validator';
import IsEmailValidator from './is_email_validator';

export default function validate(rules = {}, value) {
	let rv = [];
	Object.keys(rules)
		.map((k, i) => ruleToValidator(k, rules[k]))
		.map(validator => validator.check(value))
		.filter(errs => errs != null)
		.forEach(errs => rv = rv.concat(errs));
	return rv.length > 0 ? rv : null;
}

function ruleToValidator(rule, cfg) {
	switch(rule) {
		case 'required':
			return new RequiredValidator(cfg);
		case 'not_blank':
			return new NotBlankValidator(cfg);
		case 'is_digit':
			return new IsDigitValidator(cfg);
		case 'min':
			return new MinValidator(cfg);
		case 'max':
			return new MaxValidator(cfg);
		case 'is_date':
			return new IsDateValidator(cfg);
		case 'regex':
			return new RegExValidator(cfg);
		case 'is_email':
			return new IsEmailValidator(cfg);
		default:
			throw new Error(`Unkown validatio rule [${rule}]`);
	}
}
