import AbstractValidator from './abstract_validator';

export default class RequiredValidator extends AbstractValidator {
	check(value) {
		return value ? null : this.cfg;
	}
}
