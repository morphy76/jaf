import { html, css } from 'lit-element';
import AbstractElement from './abstract-element';
import render from '../renderers';

class JafFormControl extends AbstractElement {
	static get styles() {
		return css`
			.form-group {
				font-family: var(--jaf-label-font-family, var(--jaf-host-font-family));
				font-size: var(--jaf-label-font-size, .75em);
				display: flex;
				flex-direction: column;
				flex-grow: 1;
				justify-content: flex-start;
				align-items: stretch;
				margin-bottom: var(--jaf-group-margin-bottom, 5px);
			}
			.form-control {
				--jaf-control-host-border-radius: var(--jaf-control-border-radius, 0px);
				font-family: var(--jaf-control-font-family, var(--jaf-host-font-family));
				font-size: var(--jaf-control-font-size, 1em);
				margin-top: var(--jaf-control-margin-top, 1px);
				margin-right: var(--jaf-control-margin-right, 1px);
				margin-bottom: var(--jaf-control-margin-bottom, 0px);
				margin-left: var(--jaf-control-margin-left, 0px);
				border: var(--jaf-control-border, 1px solid darkgray);
				border-top-left-radius: var(--jaf-control-border-top-left-radius, var(--jaf-control-host-border-radius));
				border-top-right-radius: var(--jaf-control-border-top-right-radius, var(--jaf-control-host-border-radius));
				border-bottom-left-radius: var(--jaf-control-border-bottom-left-radius, var(--jaf-control-host-border-radius));
				border-bottom-right-radius: var(--jaf-control-border-bottom-right-radius, var(--jaf-control-host-border-radius));
			}
			.checkbox-control {
				font-family: var(--jaf-control-font-family, var(--jaf-host-font-family));
				font-size: var(--jaf-control-font-size, 1em);
				display: flex;
				flex-direction: row;
      			align-items: center;
				flex-grow: 1;
				justify-content: flex-start;
			}
			.color-picker {
				border: 0px !important;
			}
			.checkbox-control header {
				margin-left: 2px;
			}
			.radio {
				flex-shrink: 1;
			}
			.tooltip {
				margin-left: 5px;
				color: var(--jaf-error-notification-color, red);
			}
			.tooltip .tooltiptext {
				visibility: hidden;
				background-color: var(--jaf-error-background-color, yellow);
				color: var(--jaf-error-main-color, black);
				padding: 5px 5px 5px 5px;
				border-radius: 6px;
				position: absolute;
				z-index: 1;
			}
			.tooltip:hover .tooltiptext {
				visibility: visible;
			}
		`;
	}
	static get properties() {
		return {
			id:         { type: String },
			name:       { type: String },
			definition: { type: Object },
			errors:     { type: Object },
			labels:     { type: Object },
			value:      { type: String }
		}
	}
	constructor() {
		super();

		this.id         = '';
		this.name       = '';
		this.definition = null;
		this.errors     = {};
		this.labels     = {};
		this.value      = '';
	}
	render() {
		const required = this.definition.required ? '*' : '';

		const error = this.errors[this.name] ? html`<span class="tooltip">!!!<span class="tooltiptext">${this._printErrors(this.errors[this.name])}</span></span>` : '';
		const title = this.definition.title ? html`<label for="${this.id}">${this._translate(this.definition.title)}<sup>${required}</sup>${error}</label>` : html`${error}`;
		const description = this.definition.description ? html`<small>${this._translate(this.definition.description)}</small>` : '';

		const field = render({
			...this.definition,
			title: title,
			id: this.id,
			name: this.name,
			value: this.value,
			element: this
		});
		return html`<div class="form-group">${field}${description}</div>`;
	}
	onChange(ev) {
		ev.preventDefault();
		let event = new CustomEvent('on-form-changed', {
			detail: {
				name: ev.target.name,
				value: ev.target.value
			}
		});
		this.dispatchEvent(event);
	}
	_printErrors(errs) {
		return Array.isArray(errs)? errs.map(e => html`<p>- ${this._translate(e)}</p>`): html`${this._translate(errs)}`;
	}
	_translate(key) {
		return super._translate(key, this.labels);
	}
}

customElements.define('jaf-form-control', JafFormControl);
