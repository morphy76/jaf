import { LitElement } from 'lit-element';

export default class AbstractElement extends LitElement {
	createRenderRoot() {
		return this.attachShadow({ mode: 'closed' });
    }
    _translate(label, labels = {}) {
        const defaultLanguage = labels.default || 'en';
        const useLabels = labels.translations && labels.translations[navigator.language] ? labels.translations[navigator.language] : (
            labels.translations && labels.translations[defaultLanguage] ? labels.translations[defaultLanguage] : {}
        );
        return useLabels[label] ? useLabels[label] : label;
    }
}