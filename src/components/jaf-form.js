import { html, css } from 'lit-element';
import AbstractElement from './abstract-element';
import { v4 as uuidv4 } from 'uuid';
import './jaf-form-control';
import validate from '../validators';

export default class JafForm extends AbstractElement {
	static get styles() {
		return css`
			form {
				--jaf-host-font-family: var(--jaf-font-family, Sans-Serif);
				--jaf-host-button-margin: var(--jaf-button-margin, 10px);
				display: flex;
				flex-direction: var(--jaf-form-direction, column);
      			flex-wrap: var(--jaf-form-wrap, nowrap);
				flex-grow: var(--jaf-form-grow, 1);
				flex-shrink: var(--jaf-form-shrink, 0);
				justify-content: var(--jaf-form-justify-items, flex-start);
				align-items: var(--jaf-form-align-items, stretch);
				border: var(--jaf-form-border, none);
				margin: var(--jaf-form-margin, 0px);
				padding: var(--jaf-form-padding, 0px);
			}
			header {
				font-family: var(--jaf-title-font-family, var(--jaf-host-font-family));
				font-size: var(--jaf-title-font-size, 1em);
				font-weight: var(--jaf-title-font-weight, bold);
				flex-basis: 100%;
      			text-align: var(--jaf-title-align, center);
			}
			button {
				font-family: var(--jaf-host-font-family);
				font-size: var(--jaf-button-font-size, .75em);
				padding:0px;
				flex-grow: 1;
				flex-basis: 0;
				margin-left: var(--jaf-button-margin-left, var(--jaf-host-button-margin));
				margin-right: var(--jaf-button-margin-right, var(--jaf-host-button-margin));
				margin-top: var(--jaf-button-margin-top, var(--jaf-host-button-margin));
			}
			jaf-form-control{
				flex-basis: 0;
				flex-grow: 1;
			}
			.form-controls {
				display: flex;
				flex-direction: row;
				justify-content: space-around;
				flex-basis: 100%;
				flex-grow: 1;
			}
			.tooltip {
				margin-left: 5px;
				color: var(--jaf-error-notification-color, red);
			}
			.tooltip .tooltiptext {
				visibility: hidden;
				background-color: var(--jaf-error-background-color, yellow);
				color: var(--jaf-error-main-color, black);
				padding: 5px 5px 5px 5px;
				border-radius: 6px;
				position: absolute;
				z-index: 1;
			}
			.tooltip:hover .tooltiptext {
				visibility: visible;
			}
		`;
	}
	static get properties() {
		return {
			id:         { type: String },
			title:      { type: String },
			definition: { type: Object },
			src:        { type: String },
			useValues:  { type: Object, attribute: false },
			submit:     { type: String },
			cancel:     { type: String }
		}
	}
	constructor() {
		super();

		this.id            = uuidv4();
		this.title         = '';
		this.src           = '';
		this.definition    = {schema: {}, errors: { values: {}, messages: {} }, initial_values: {}};
		this.useValues     = {};
		this.submit        = 'label.submit';
		this.cancel        = 'label.cancel';
	}
	firstUpdated(changedProperties) {
		if(this.src) {
			fetch(this.src)
				.then(response => response.json())
				.then(def => {
					this.definition = def;
					return true;
				})
				.then(b => this._normalizeDefinition());
		} else {
			this._normalizeDefinition();
		}
	}
	render() {
		const messages = this.definition.errors && this.definition.errors.messages? this.definition.errors.messages: {};
		const form_messages = this.definition.errors && this.definition.errors.form_messages? html`<span class="tooltip">!!!<span class="tooltiptext">${this._printErrors()}</span></span>` : '';
		const title = this.title? html`<header>${this._translate(this.title)}</header>`: '';
		
		return html`<form onSubmit="return false;">
			${title}
			${Object.keys(this.definition.schema)
				.map((k, i) => html`<jaf-form-control id="fld_${this.id}_${k}" name="${k}" value="${this.useValues[k] ? this.useValues[k] : ''}"
													  definition="${JSON.stringify(this.definition.schema[k])}"
													  errors="${JSON.stringify(messages)}" labels="${JSON.stringify(this.definition.labels)}"
													  @on-form-changed="${this._onChange}"></jaf-form-control>`)}
			<div class="form-controls">
				<button @click=${this._validateAndSubmit}>${this._translate(this.submit)}${form_messages}</button>
				<button @click=${this._handleCancel}>${this._translate(this.cancel)}</button>
			</div>
		</form>`;
	}
	addErrors(errorDefinition) {
		this.definition = {...this.definition, errors: errorDefinition};
	}
	_validateAndSubmit() {
		this._validate(() => this._handleSubmit(), errorMessages => {
			this.definition = {...this.definition, errors: { messages: errorMessages }};
		})
	}
	_handleSubmit() {
		this.definition= {...this.definition, errors: { values: {}, messages: {}}};
		const event = new CustomEvent('on-submit', {
			detail: {...this.useValues}
		});
		this.dispatchEvent(event);
		this._resetForm();
	}
	_handleCancel() {
		this.definition= {...this.definition, errors: { values: {}, messages: {}}};
		this._resetForm();
	}
	_validate(onSuccess, onError) {
		if(this.definition.validate) {
			let useErrs = {};
			Object.keys(this.definition.schema)
				.map((k, i) => useErrs = {...useErrs, [k]: validate(this.definition.validate[k], this.useValues[k]) });
			if(Object.values(useErrs).map(v => v !== null).filter(v => v).length > 0) {
				return onError(useErrs);
			}
		}
		onSuccess();
	}
	_resetForm() {
		this.useValues = {...this.definition.initial_values, ...this.definition.errors.values};
	}
	_onChange(ev) {
		this.useValues = {...this.useValues, [ev.detail.name]: ev.detail.value};
	}
	_normalizeDefinition() {
		if(!this.definition.initial_values) {
			this.definition.initial_values = {};
		}

		if(!this.definition.errors) {
			this.definition = {...this.definition, errors: { values: {}, messages: {}}};
		} else {
			if(!this.definition.errors.values) {
				this.definition = {...this.definition, errors: { ...this.definition.errors, values: {}}};
			}
			if(!this.definition.errors.messages) {
				this.definition = {...this.definition, errors: { ...this.definition.errors, messages: {}}};
			}
		}

		this._resetForm();
	}
	_printErrors() {
		const errs = this.definition.errors.form_messages;
		return Array.isArray(errs)? errs.map(e => html`<p>- ${this._translate(e)}</p>`): html`${this._translate(errs)}`;
	}
	_translate(key) {
		return super._translate(key, this.definition.labels);
	}
}
