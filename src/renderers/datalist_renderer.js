import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class DatalistRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}<input list="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}"
									type="datalist" class="form-control"
									@keyup="${this.cfg.element.onChange}" @change="${this.cfg.element.onChange}"
									.value="${ifDefined(this.cfg.value)}"
									value="${ifDefined(this.cfg.value)}">
					<datalist id="${this.cfg.id}">
						${this._ui('elements').map(element => {
							return html`<option value="${element}">`
						})}
					</datalist>
					`;
	}
}
