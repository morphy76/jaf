import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class CheckboxRenderer extends AbstractRenderer {
	render() {
		return html`<div class="checkbox-control"><input id="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}"
									type="checkbox" class="form-control"
									@change="${ev => this._onChange(ev, this.cfg.element, this._ui('value'))}"
									?checked="${ifDefined(this.cfg.value === this._ui('value') ? 'checked' : undefined)}"
									value="${ifDefined(this._ui('value'))}">${this.cfg.title}</div>`;
	}
	_onChange(ev, element, useValue) {
		ev.preventDefault();
		let event = new CustomEvent('on-form-changed', {
			detail: {
				name: ev.target.name,
				value: ev.target.checked ? useValue : undefined
			}
		});
		element.dispatchEvent(event);
	}
}