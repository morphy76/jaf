export default class AbstractRenderer {
	constructor(cfg = {}) {
		this.cfg = cfg;
	}
	render() {
		throw new Error('Cannot invoke an abstract method');
	}
	_ui(key) {
		return this.cfg.ui && this.cfg.ui[key] ? this.cfg.ui[key] : undefined;
	}
	_translate(key) {
		return this.cfg.element._translate(key);
	}
}
