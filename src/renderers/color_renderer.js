import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class ColorRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}<input id="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}"
									type="color" class="form-control color-picker"
									@change="${this.cfg.element.onChange}"
									.value="${ifDefined(this.cfg.value)}"
									value="${ifDefined(this.cfg.value)}">`;
	}
}