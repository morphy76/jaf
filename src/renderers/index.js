import TextAreaRenderer from './textarea_renderer';
import InputRenderer from './input_renderer';
import CheckboxRenderer from './checkbox_renderer';
import RadioRenderer from './radio_renderer';
import SelectRenderer from './select_renderer';
import DatalistRenderer from './datalist_renderer';
import ColorRenderer from './color_renderer';

export default function render(cfg) {
	return typeToRenderer(cfg).render();
}

function typeToRenderer(cfg) {
	switch(cfg.type) {
		case 'color':
			return new ColorRenderer(cfg);
		case 'datalist':
			return new DatalistRenderer(cfg);
		case 'select':
			return new SelectRenderer(cfg);
		case 'radio':
			return new RadioRenderer(cfg);
		case 'checkbox':
			return new CheckboxRenderer(cfg);
		case 'textarea':
			return new TextAreaRenderer(cfg);
		default:
			return new InputRenderer(cfg);
	}
}
