import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class RadioRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}<select id="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}"
									class="form-control" @change="${ev => this._onChange(ev, this.cfg.element)}"
									size="${ifDefined(this._ui('size'))}" ?multiple="${this._ui('multiple')}">
						${this._ui('elements').map(element => {
							return html`<option value="${element.value}" selected="${ifDefined(this._isSelected(element.value) ? 'selected' : undefined)}">
										${this._translate(element.label)}</option>`
						})}
					</select>
		`;
	}
	_onChange(ev, element) {
		ev.preventDefault();
		const useVal = [];
		for (var i = 0, iLen = ev.target.options.length; i < iLen; i++) {
			if(ev.target.options[i].selected) {
				useVal.push(ev.target.options[i].value);
			}
		}
		let event = new CustomEvent('on-form-changed', {
			detail: {
				name: ev.target.name,
				value: useVal
			}
		});
		element.dispatchEvent(event);
	}
	_isSelected(val) {
		const currentValue = this.cfg.value ? JSON.parse(this.cfg.value) : [];
		if(Array.isArray(currentValue)) {
			return currentValue.includes(val);
		} else {
			return currentValue == val;
		}
	}
}