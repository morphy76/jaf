import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class InputRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}<input id="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}"
									type="${this.cfg.type}" class="form-control"
									@keyup="${this.cfg.element.onChange}" @change="${this.cfg.element.onChange}"
									placeholder="${ifDefined(this.cfg.placeholder ? this._translate(this.cfg.placeholder) : undefined)}"
									min="${ifDefined(this._ui('min'))}"
									max="${ifDefined(this._ui('max'))}"
									pattern="${ifDefined(this._ui('pattern'))}"
									step="${ifDefined(this._ui('step'))}"
									.value="${ifDefined(this.cfg.value)}"
									value="${ifDefined(this.cfg.value)}">`;
	}
}