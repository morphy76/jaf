import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class RadioRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}
			${this._ui('elements').map(element => {
				return html`<input type="radio" name="${this.cfg.name}" class="form-control radio" value="${element.value}"
								checked="${ifDefined(this.cfg.value == element.value ? 'checked' : undefined)}"
								@change="${this.cfg.element.onChange}">
							<label>${this._translate(element.label)}</label>
				`
			})}
		`;
	}
}