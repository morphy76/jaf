import { html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined';
import AbstractRenderer from './abstract_renderer';

export default class TextAreaRenderer extends AbstractRenderer {
	render() {
		return html`${this.cfg.title}<textarea id="${this.cfg.id}" name="${this.cfg.name}" ?required="${this.cfg.required}" class="form-control"
									   @keyup="${this.cfg.element.onChange}" @change="${this.cfg.element.onChange}"
									   placeholder="${ifDefined(this.cfg.placeholder ? this._translate(this.cfg.placeholder) : undefined)}"
									   rows="${ifDefined(this._ui('rows'))}" cols="${ifDefined(this._ui('cols'))}"
									   .value="${ifDefined(this.cfg.value)}">
									   ${this.cfg.value}
									</textarea>`;
	}
}